/*
 * TrackDriver.h
 *
 *  Created on: 12 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef DCCTOOLKIT_TRACKDRIVER_H_
#define DCCTOOLKIT_TRACKDRIVER_H_

#include "Arduino.h"

#include "SignalGenerator.h"
#include "Constants.h"

#include <SystemToolkit.h>

class TrackDriver
{
	private:

	uint16_t powerPinNumber;

	SignalGenerator *signalGenerator;
	CurrentSensor *currentSensor;

	uint16_t ackBaseCurrent;
	uint16_t ackThresholdCurrent;

	uint8_t retryCount;

	public:

	TrackDriver(uint16_t aPowerPinNumber, SignalGenerator *generator,
			CurrentSensor *sensor);

	void setRetryCount(uint8_t value)
	{
		retryCount = value;
	}

	uint8_t getRetryCount()
	{
		return retryCount;
	}

	void setAckThresholdCurrent(uint16_t value)
	{
		ackThresholdCurrent = value;
	}

	uint16_t getAckThresholdCurrent()
	{
		return ackThresholdCurrent;
	}

	void start();
	void stop();

	//
	// General purpose operations
	//

	void powerOff();
	void powerOn();

	//
	// OperationMode related operations
	//

	void broadcastEmergencyStop(bool forward);

	void sendEmergencyStop(uint16_t address, bool forward);

	void sendStop(uint16_t address, bool forward);

	void send28StepSpeedAndDirection(uint16_t address, uint8_t speed,
			bool forward);

	void send128StepSpeedAndDirection(uint16_t address, uint8_t speed,
			bool forward);

	void sendFunctionGroup(uint16_t address, FUNCTION_GROUPS group,
			uint8_t functionMask);

	void operationWriteByte(uint16_t address, uint16_t cv, uint8_t value);

	void operationWriteBit(uint16_t address, uint16_t cv, uint8_t bitIndex,
			bool value);

	//
	// ServiceMode related operations
	//

	bool directWriteByte(uint16_t cv, uint8_t value);
	bool directVerifyByte(uint16_t cv, uint8_t value);

	bool directWriteBit(uint16_t cv, uint8_t bitIndex, bool value);
	bool directVerifyBit(uint16_t cv, uint8_t bitIndex, bool value);

	bool physicalRegisterWrite(byte registerNumber, byte value);
	bool physicalRegisterVerify(byte registerNumber, byte value);

	bool pagedModeWrite(byte registerNumber, byte value);
	bool pagedModeVerify(byte registerNumber, byte value);

	//
	// Ack sensor management
	//

	void prepareForAck(uint32_t duration);
	bool waitAck(uint32_t prologDuration,uint32_t commandDuration, uint32_t epilogDuration);

	//
	// High level commands
	//

	bool readCV(uint16_t cv, uint8_t *value);
	bool readCVBit(uint16_t cv, uint8_t bitNumber, bool *value);
	bool writeCV(uint16_t cv, uint8_t value);
	bool writeCVBit(uint16_t cv, uint8_t bitNumber, bool value);
};

#endif /* DCCTOOLKIT_TRACKDRIVER_H_ */
