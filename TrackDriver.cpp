/*
 * TrackDriver.cpp
 *
 *  Created on: 12 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "TrackDriver.h"

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

TrackDriver::TrackDriver(uint16_t aPowerPinNumber, SignalGenerator *generator,
		CurrentSensor *sensor)
{
	powerPinNumber = aPowerPinNumber;
	signalGenerator = generator;
	currentSensor = sensor;
	ackBaseCurrent = 0;
	ackThresholdCurrent = DCC_BASIC_ACK_CURRENT_LOAD_MILLIAMP;
	retryCount = 5;
}

//==============================================================================
//
// Public operations
//
//==============================================================================

//------------------------------------------------------------------------------

void TrackDriver::start()
{
	pinMode(powerPinNumber, OUTPUT);
	digitalWrite(powerPinNumber, 0);

	signalGenerator->start();
	currentSensor->start();
	ackBaseCurrent = 0;
}

//------------------------------------------------------------------------------

void TrackDriver::stop()
{
	signalGenerator->stop();
	currentSensor->stop();
}

//------------------------------------------------------------------------------
// General purpose operations
//------------------------------------------------------------------------------

void TrackDriver::powerOff()
{
	digitalWrite(powerPinNumber, 0);
}

//------------------------------------------------------------------------------

void TrackDriver::powerOn()
{
	digitalWrite(powerPinNumber, 1);
}

//------------------------------------------------------------------------------
// OperationMode related operations
//------------------------------------------------------------------------------

void TrackDriver::broadcastEmergencyStop(bool forward)
{
	signalGenerator->cancelQueue();
	signalGenerator->queuePacket(
			Packet::speed28StepAndDirection(DCC_SHORT_ADDRESS_BROADCAST, 2,
					forward), 10);
}

//------------------------------------------------------------------------------

void TrackDriver::sendEmergencyStop(uint16_t address, bool forward)
{
	signalGenerator->cancelAnyPermanentPacket(address);

	if (address <= 127)
		signalGenerator->queuePermanentPacket(
				Packet::speed28StepAndDirection(address, 2, forward));
	else
		signalGenerator->queuePermanentPacket(
				Packet::speed128StepAndDirection(address, 2, forward));
}

//------------------------------------------------------------------------------

void TrackDriver::sendStop(uint16_t address, bool forward)
{
	signalGenerator->cancelAnyPermanentPacket(address);

	if (address <= 127)
		signalGenerator->queuePermanentPacket(
				Packet::speed28StepAndDirection(address, 0, forward));
	else
		signalGenerator->queuePermanentPacket(
				Packet::speed128StepAndDirection(address, 0, forward));
}

//------------------------------------------------------------------------------

void TrackDriver::send28StepSpeedAndDirection(uint16_t address, uint8_t speed,
		bool forward)
{
	signalGenerator->cancelAnyPermanentPacket(address);

	if (address <= 127)
		signalGenerator->queuePermanentPacket(
				Packet::speed28StepAndDirection(address, speed, forward));
	else
		signalGenerator->queuePermanentPacket(
				Packet::speed128StepAndDirection(address, speed, forward));
}

//------------------------------------------------------------------------------

void TrackDriver::send128StepSpeedAndDirection(uint16_t address, uint8_t speed,
		bool forward)
{
	signalGenerator->cancelAnyPermanentPacket(address);

	signalGenerator->queuePermanentPacket(
			Packet::speed128StepAndDirection(address, speed, forward));
}

//------------------------------------------------------------------------------

void TrackDriver::sendFunctionGroup(uint16_t address, FUNCTION_GROUPS group,
		uint8_t functionMask)
{
	signalGenerator->queuePacket(
			Packet::functionGroup(address, group, functionMask), 10);
}

//------------------------------------------------------------------------------

void TrackDriver::operationWriteByte(__attribute__((unused))uint16_t address,
		__attribute__((unused))uint16_t cv, __attribute__((unused))uint8_t value)
{
}

//------------------------------------------------------------------------------

void TrackDriver::operationWriteBit(__attribute__((unused))uint16_t address,
		__attribute__((unused))uint16_t cv, __attribute__((unused))uint8_t bitIndex,
		__attribute__((unused))bool value)
{
}

//------------------------------------------------------------------------------
// ServiceMode related operations
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

bool TrackDriver::directWriteByte(uint16_t cv, uint8_t value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	//
	// Start by entering service mode properly
	//

	if (!signalGenerator->enterServiceMode())
		return false;

	//
	// Issue 3 reset packets followed by 5 write and 1 reset packet
	// We then wait during 10 reset packet until write occurs
	//

	PACKET_BUFFER *firstReset = Packet::reset();
	PACKET_BUFFER *write = Packet::directWriteByte(cv, value);
	PACKET_BUFFER *secondReset = Packet::reset();

	uint32_t adjustmentBitDuration = signalGenerator->packetDuration(
			firstReset);
	uint32_t prologBitDuration = 3 * adjustmentBitDuration;
	uint32_t commandBitDuration = 5 * signalGenerator->packetDuration(write);
	uint32_t epilogBitDuration = 10
			* signalGenerator->packetDuration(secondReset);

	prepareForAck(prologBitDuration);

	signalGenerator->lockQueue();

	signalGenerator->queuePacket(firstReset, 3);
	signalGenerator->queuePacket(write, 5);
	signalGenerator->queuePacket(secondReset, 10);

	signalGenerator->unlockQueue();

	bool result = waitAck(prologBitDuration,
			commandBitDuration + adjustmentBitDuration, epilogBitDuration);

	//
	// Before returning the result, leave service mode
	//

	signalGenerator->enterOperationMode();

	return result;
}

//------------------------------------------------------------------------------

bool TrackDriver::directVerifyByte(uint16_t cv, uint8_t value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	//
	// Start by entering service mode properly
	//

	if (!signalGenerator->enterServiceMode())
		return false;

	//
	// Queue 3 reset packets followed by 5 verify and 6 reset packet
	//

	PACKET_BUFFER *firstReset = Packet::reset();
	PACKET_BUFFER *verify = Packet::directVerifyByte(cv, value);
	PACKET_BUFFER *secondReset = Packet::reset();

	uint32_t adjustmentBitDuration = signalGenerator->packetDuration(
			firstReset);
	uint32_t prologBitDuration = 3 * adjustmentBitDuration;
	uint32_t commandBitDuration = 5 * signalGenerator->packetDuration(verify);
	uint32_t epilogBitDuration = 6
			* signalGenerator->packetDuration(secondReset);

	prepareForAck(prologBitDuration);

	signalGenerator->lockQueue();

	signalGenerator->queuePacket(firstReset, 3);
	signalGenerator->queuePacket(verify, 5);
	signalGenerator->queuePacket(secondReset, 6);

	signalGenerator->unlockQueue();

	bool result = waitAck(prologBitDuration,
			commandBitDuration + adjustmentBitDuration, epilogBitDuration);

	//
	// Before returning the result, leave service mode
	//

	signalGenerator->enterOperationMode();

	return result;
}

//------------------------------------------------------------------------------

bool TrackDriver::directWriteBit(uint16_t cv, uint8_t bitIndex, bool value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	//
	// Start by entering service mode properly
	//

	if (!signalGenerator->enterServiceMode())
		return false;

	//
	// Issue 3 reset packets followed by 5 write and 1 reset packet.
	// We then wait during 10 reset packet until the write occurs.
	//

	PACKET_BUFFER *firstReset = Packet::reset();
	PACKET_BUFFER *write = Packet::directWriteBit(cv, bitIndex, value);
	PACKET_BUFFER *secondReset = Packet::reset();

	uint32_t adjustmentBitDuration = signalGenerator->packetDuration(
			firstReset);
	uint32_t prologBitDuration = 3 * adjustmentBitDuration;
	uint32_t commandBitDuration = 5 * signalGenerator->packetDuration(write);
	uint32_t epilogBitDuration = 10
			* signalGenerator->packetDuration(secondReset);

	prepareForAck(prologBitDuration);

	signalGenerator->lockQueue();

	signalGenerator->queuePacket(firstReset, 3);
	signalGenerator->queuePacket(write, 5);
	signalGenerator->queuePacket(secondReset, 10);

	signalGenerator->unlockQueue();

	bool result = waitAck(prologBitDuration,
			commandBitDuration + adjustmentBitDuration, epilogBitDuration);

	//
	// Before returning the result, leave service mode
	//

	signalGenerator->enterOperationMode();

	return result;
}

//------------------------------------------------------------------------------

bool TrackDriver::directVerifyBit(uint16_t cv, uint8_t bitIndex, bool value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	//
	// Start by entering service mode properly
	//

	if (!signalGenerator->enterServiceMode())
		return false;

	//
	// Issue 3 reset packets followed by 5 verify and 6 reset packet
	//

	PACKET_BUFFER *firstReset = Packet::reset();
	PACKET_BUFFER *verify = Packet::directVerifyBit(cv, bitIndex, value);
	PACKET_BUFFER *secondReset = Packet::reset();

	uint32_t adjustmentBitDuration = signalGenerator->packetDuration(
			firstReset);
	uint32_t prologBitDuration = 3 * adjustmentBitDuration;
	uint32_t commandBitDuration = 5 * signalGenerator->packetDuration(verify);
	uint32_t epilogBitDuration = 6
			* signalGenerator->packetDuration(secondReset);

	prepareForAck(prologBitDuration);

	signalGenerator->lockQueue();

	signalGenerator->queuePacket(firstReset, 3);
	signalGenerator->queuePacket(verify, 5);
	signalGenerator->queuePacket(secondReset, 6);

	signalGenerator->unlockQueue();

	bool result = waitAck(prologBitDuration,
			commandBitDuration + adjustmentBitDuration, epilogBitDuration);

	//
	// Before returning the result, leave service mode
	//

	signalGenerator->enterOperationMode();

	return result;
}

//------------------------------------------------------------------------------

bool TrackDriver::physicalRegisterWrite(__attribute__((unused))byte registerNumber,
		__attribute__((unused))byte value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	return false;
}

//------------------------------------------------------------------------------

bool TrackDriver::physicalRegisterVerify(__attribute__((unused))byte registerNumber,
		__attribute__((unused))byte value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	return false;
}

//------------------------------------------------------------------------------

bool TrackDriver::pagedModeWrite(__attribute__((unused))byte registerNumber,
		__attribute__((unused))byte value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	return false;
}

//------------------------------------------------------------------------------

bool TrackDriver::pagedModeVerify(__attribute__((unused))byte registerNumber,
		__attribute__((unused))byte value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	return false;
}

//------------------------------------------------------------------------------
// Ack sensor management
//------------------------------------------------------------------------------

void TrackDriver::prepareForAck(uint32_t duration)
{
	ackBaseCurrent = currentSensor->senseDurationSample(duration);
}

//------------------------------------------------------------------------------

bool TrackDriver::waitAck(uint32_t prologDuration, uint32_t commandDuration,
		uint32_t epilogDuration)
{
	uint32_t peakCurrent = ackBaseCurrent + ackThresholdCurrent;

	uint32_t end = millis() + prologDuration + commandDuration + epilogDuration;
	uint32_t duration = 0;

#ifdef DCC_DEBUG_ACK
	Serial.print("Base ");
	Serial.print(ackBaseCurrent);
	Serial.print(" delay ");
	Serial.print(prologDuration + commandDuration + epilogDuration);
#endif

	while (millis() < end)
	{
		uint32_t sensed = currentSensor->sensePeakDuration(10, peakCurrent);

#ifdef DCC_DEBUG_ACK
		Serial.print("/");
		Serial.print(sensed);
#endif

		duration += sensed;

		if (duration >= DCC_BASIC_ACK_DURATION_MILLISEC_MIN)
			return true;
	}

	/*
	 uint8_t step = DCC_BASIC_ACK_SAMPLES_PER_CHECK
	 / DCC_BASIC_ACK_SAMPLES_PER_POINT;
	 uint16_t sample;

	 while (step--)
	 {
	 sample = currentSensor->sense(DCC_BASIC_ACK_SAMPLES_PER_POINT);

	 if ((sample > ackBaseCurrent)
	 && (sample - ackBaseCurrent
	 > DCC_BASIC_ACK_CURRENT_LOAD_MILLIAMP))
	 {
	 if (step)
	 {
	 while ((step--) && (sample > ackBaseCurrent)
	 && (sample - ackBaseCurrent
	 > DCC_BASIC_ACK_CURRENT_LOAD_MILLIAMP))
	 {
	 sample = currentSensor->sense(
	 DCC_BASIC_ACK_SAMPLES_PER_POINT);
	 }
	 }

	 return true;
	 }
	 }
	 */

	return false;
}

//------------------------------------------------------------------------------
// High level commands
//------------------------------------------------------------------------------

bool TrackDriver::readCV(uint16_t cv, uint8_t *result)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	//
	// Verify each bit of the CV with a fixed 1. A successful verify means
	// the effective bit is set. A failed one mean either the bit was not set
	// OR the command failed, but we can't be sure...
	//

	int count = retryCount;

	while (count--)
	{
		uint8_t value = 0;
		uint8_t mask = 0x01;
		for (uint8_t index = 0; index < 8; index++)
		{
			delay(10);
			bool bitSet = directVerifyBit(cv, index, true);

			if (bitSet)
				value |= mask;

			mask = mask << 1;
		}

		//
		// As we don't really know whether we really have the CV value, we now
		// issue a full CV value verify to be sure...
		//

		*result = value;

		if (directVerifyByte(cv, value))
			return true;
	}

	return false;
}

//------------------------------------------------------------------------------

bool TrackDriver::readCVBit(uint16_t cv, uint8_t bitNumber, bool *value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	int count = retryCount;

	while (count--)
	{
		if (directVerifyBit(cv, bitNumber, true))
		{
			*value = true;
			return true;
		}
		if (directVerifyBit(cv, bitNumber, false))
		{
			*value = false;
			return true;
		}
	}

	return false;
}

//------------------------------------------------------------------------------

bool TrackDriver::writeCV(uint16_t cv, uint8_t value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	if (!directWriteByte(cv, value))
		return false;

	return directVerifyByte(cv, value);
}

//------------------------------------------------------------------------------

bool TrackDriver::writeCVBit(uint16_t cv, uint8_t bitNumber, bool value)
{
	if (!signalGenerator->isStarted())
		return false;

	if (!signalGenerator->isServiceModeAllowed())
		return false;

	if (!directWriteBit(cv, bitNumber, value))
		return false;

	return directVerifyBit(cv, bitNumber, value);
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

//------------------------------------------------------------------------------
