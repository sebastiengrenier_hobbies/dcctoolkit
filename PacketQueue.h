/*
 * PacketQueue.h
 *
 *  Created on: 13 mars 2021
 *      Author: Sébastien GRENIER
 */

#ifndef DCCTOOLKIT_PACKETQUEUE_H_
#define DCCTOOLKIT_PACKETQUEUE_H_

#include "Arduino.h"
#include "Packet.h"

//==============================================================================
//
// Internal linked list queue definition
//
//==============================================================================

#define PACKET_QUEUE_PAGE_SIZE 16

typedef struct
{
	PACKET_BUFFER *packet;
	uint8_t transmitionCount;
	bool permanent;
	bool asAWhole;
	void *previous;
	void *next;
} PACKET_QUEUE_ELEMENT;

typedef struct
{
	PACKET_QUEUE_ELEMENT *cache;
	void *next;
} PACKET_QUEUE_PAGE;

//==============================================================================

class PacketQueue
{
	private:

	PACKET_QUEUE_PAGE *firstPage;
	PACKET_QUEUE_ELEMENT *firstElement;
	PACKET_QUEUE_ELEMENT *lastElement;
	PACKET_QUEUE_ELEMENT *lockedFirstElement;

	bool locked;

	public:

	PacketQueue();
	virtual ~PacketQueue();

	void release(PACKET_QUEUE_ELEMENT *element);

	bool isEmpty();

	void clearAll();
	void clearForAddress(uint16_t address, bool permanent);

	void lock();
	void unlock();

	void queue(PACKET_BUFFER *buffer, int count, bool permanent, bool asAWhole);

	PACKET_QUEUE_ELEMENT* getFirst();
	PACKET_QUEUE_ELEMENT* getNext(PACKET_QUEUE_ELEMENT *current);

	protected:

	PACKET_QUEUE_PAGE* allocatePage();

	PACKET_QUEUE_ELEMENT* allocate(PACKET_BUFFER *buffer, int count,
			bool permanent, bool asAWhole);
};

#endif /* DCCTOOLKIT_PACKETQUEUE_H_ */
