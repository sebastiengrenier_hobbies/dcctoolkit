/*
 * SignalGenerator.cpp
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 */

#include "SignalGenerator.h"

#include "Constants.h"
#include "Packet.h"

//------------------------------------------------------------------------------

static PACKET_BUFFER IDLE;
static PACKET_BUFFER RESET;
//------------------------------------------------------------------------------

#ifdef DCC_DEBUG
static char HEXCHARS[16] =
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
				'E', 'F' };

void DumpPacket(byte *buffer, byte count)
{
	if (buffer[0] != 0xFF)
	{
		for (byte index = 0; index < count; index++)
		{
			byte b = buffer[index];
			Serial.print(HEXCHARS[b >> 4]);
			Serial.print(HEXCHARS[b & 0x0F]);
			Serial.print(" ");
		}
		Serial.println(" ");
	}
}
#else
#define DumpPacket(A,B)
#endif

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

SignalGenerator::SignalGenerator(uint8_t aTimerIndex, uint8_t aPinNumber,
		bool aServiceModeAllowed)
{
	signalPinNumber = aPinNumber;

	IDLE.address = DCC_SHORT_ADDRESS_RESERVED_IDLE;
	IDLE.buffer[0] = DCC_SHORT_ADDRESS_RESERVED_IDLE;
	IDLE.buffer[1] = 0;
	IDLE.buffer[2] = DCC_SHORT_ADDRESS_RESERVED_IDLE;
	IDLE.size = 3;

	RESET.address = DCC_SHORT_ADDRESS_BROADCAST;
	RESET.buffer[0] = DCC_SHORT_ADDRESS_BROADCAST;
	RESET.buffer[1] = 0;
	RESET.buffer[2] = DCC_SHORT_ADDRESS_BROADCAST;
	RESET.size = 3;

	started = false;
	timerIndex = aTimerIndex;

	timer = Timer::getInstance(timerIndex);
	timer->setEventHandler(this);

	//
	// Depending on the timer resolution, we store appropriate settings
	//

	switch (timer->getResolution())
	{
		case 8:
		{
			if (timerIndex == 0)
			{
				bit0PulseDuration = DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_0;
				bit0TotalDuration = DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_0;
				bit1PulseDuration = DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_0;
				bit1TotalDuration = DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_0;
			}
			else
			{
				bit0PulseDuration = DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_2;
				bit0TotalDuration = DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_2;
				bit1PulseDuration = DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_2;
				bit1TotalDuration = DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_2;
			}

			break;
		}
		case 16:
		{

			bit0PulseDuration = DCC_ZERO_BIT_PULSE_DURATION_16_BIT_TIMER;
			bit0TotalDuration = DCC_ZERO_BIT_TOTAL_DURATION_16_BIT_TIMER;
			bit1PulseDuration = DCC_ONE_BIT_PULSE_DURATION_16_BIT_TIMER;
			bit1TotalDuration = DCC_ONE_BIT_TOTAL_DURATION_16_BIT_TIMER;

			break;
		}
	}

	serviceModeAllowed = aServiceModeAllowed;

	serviceMode = false;

	preambleDuration = DCC_DEFAULT_OPERATION_MODE_PREAMBLE_DURATION;
	preambleBitCount = preambleDuration;

	currentQueueElement = NULL;

	dataByteIndex = 0;
	dataByteCount = 0;
	currentBuffer = NULL;

	currentDataByte = 0;
	currentDataBitCount = 0;

	currentState = STATE_PREAMBLE;
}

//------------------------------------------------------------------------------

SignalGenerator::~SignalGenerator()
{
	stop();
	delete timer;
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void SignalGenerator::start()
{
	//
	// Refuse to start when already started
	//

	if (started)
		return;

	//
	// Let's start the engine
	//

	serviceMode = false;

	preambleDuration = DCC_DEFAULT_OPERATION_MODE_PREAMBLE_DURATION;
	preambleBitCount = preambleDuration;

	currentQueueElement = NULL;

	currentBuffer = NULL;
	dataByteIndex = 0;

	currentDataByte = 0;
	dataByteCount = 0;
	currentDataBitCount = 0;

	currentState = STATE_PREAMBLE;

#ifdef DCC_DEBUG

	//
	// Enable output pins for debug
	//

	pinMode(DCC_DEBUG_IDLE_PIN, OUTPUT);
	pinMode(DCC_DEBUG_PREAMBLE_PIN, OUTPUT);
	pinMode(DCC_DEBUG_BYTE_START_PIN, OUTPUT);
	pinMode(DCC_DEBUG_DATA_0_PIN, OUTPUT);
	pinMode(DCC_DEBUG_DATA_1_PIN, OUTPUT);
	pinMode(DCC_DEBUG_PACKET_END_PIN, OUTPUT);

	digitalWrite(DCC_DEBUG_IDLE_PIN, LOW);
	digitalWrite(DCC_DEBUG_PREAMBLE_PIN, LOW);
	digitalWrite(DCC_DEBUG_BYTE_START_PIN, LOW);
	digitalWrite(DCC_DEBUG_DATA_0_PIN, LOW);
	digitalWrite(DCC_DEBUG_DATA_1_PIN, LOW);
	digitalWrite(DCC_DEBUG_PACKET_END_PIN, LOW);

#endif

	//
	// Start timer & interrupt
	//
	// Set the required timer to FAST PWM with :
	// - overall cycle duration in Comparator "A" (OCRxA)
	// - half cycle duration in comparator "B" (OCRxB)
	// - automatic inverting toggle on half cycle
	//
	// We also enable interruption on COMPARE "B" match. We will be called in the middle
	// of the current bit being generated. For each interrupt, we will define
	// what the next bit will be and reload OCRxA and OCRxB with appropriate
	// values for the next cycle. These registers are double buffered and will
	// be really loaded into the timer when current cycle is finished. This way,
	// there won't be any loss between bits and the appropriate wave form will
	// be generated.
	//

	timer->setOutputCompareAMode(COMPARE_MODE_NONE);
	timer->setOutputCompareBMode(COMPARE_MODE_CLEAR_ON_MATCH);

	timer->setOutputCompareA(bit1TotalDuration);
	timer->setOutputCompareB(bit1PulseDuration);

	if (timer->getResolution() == 8)
	{
		timer->setTimerMode(TIMER_8BIT_MODE_FAST_PWM_OCR);
		if (timerIndex != 2)
			timer->setClockMode(CLOCK_MODE_INTERNAL_PRESCALE_64);
		else
			timer->setClockMode(CLOCK_MODE_TIMER2_INTERNAL_PRESCALE_32);
	}
	else
	{
		timer->setTimerMode(TIMER_16BIT_MODE_FAST_PWM_OCR);
		timer->setClockMode(CLOCK_MODE_INTERNAL_PRESCALE_1);
	}

	timer->enableOutputCompareBInterrupt(true);
	timer->start();

	pinMode(signalPinNumber, OUTPUT);

	started = true;
}

//------------------------------------------------------------------------------

void SignalGenerator::stop()
{
	if (!started)
		return;

	timer->stop();
	cancelQueue();

	started = false;
}

//------------------------------------------------------------------------------

uint32_t SignalGenerator::packetDuration(PACKET_BUFFER *aPacket)
{
	uint32_t result = 0;

	result += preambleDuration * DCC_ONE_BIT_TOTAL_DURATION_MICROSECONDS;

	for (int i = 0; i < aPacket->size; i++)
	{
		// Start bit
		result += DCC_ZERO_BIT_TOTAL_DURATION_MICROSECONDS;

		// Each data bit

		uint8_t mask = 0x01;
		uint8_t data = aPacket->buffer[i];
		for (int b = 0; b < 8; b++)
		{
			if (data & mask)
				result += DCC_ONE_BIT_TOTAL_DURATION_MICROSECONDS;
			else
				result += DCC_ZERO_BIT_TOTAL_DURATION_MICROSECONDS;

			mask = mask << 1;
		}
	}

	result += DCC_ONE_BIT_TOTAL_DURATION_MICROSECONDS; // End packet

	return result / 1000; // From micro to millis
}

//------------------------------------------------------------------------------

void SignalGenerator::lockQueue()
{
	queue.lock();
}

//------------------------------------------------------------------------------

void SignalGenerator::unlockQueue()
{
	queue.unlock();
}

//------------------------------------------------------------------------------
void SignalGenerator::cancelQueue()
{
	if (!started)
		return;

	queue.clearAll();
}

//------------------------------------------------------------------------------

void SignalGenerator::queuePermanentPacket(PACKET_BUFFER *aPacket)
{
	if (!started)
		return;

	queue.queue(aPacket, 0, true, false);
}

//------------------------------------------------------------------------------

void SignalGenerator::cancelAnyPermanentPacket(uint16_t address)
{
	queue.clearForAddress(address, true);
}

//------------------------------------------------------------------------------

void SignalGenerator::queuePacket(PACKET_BUFFER *aPacket, byte count)
{
	if (!started)
		return;

	queue.queue(aPacket, count, false, serviceMode);
}

//------------------------------------------------------------------------------

void SignalGenerator::sendPacket(PACKET_BUFFER *aPacket, byte count)
{
	if (!started)
		return;

	queue.clearAll();
	queue.queue(aPacket, count, false, serviceMode);

	while (!queue.isEmpty())
	{
		delay(1);
	}
}

//------------------------------------------------------------------------------

bool SignalGenerator::enterOperationMode()
{
	if (!started)
		return false;

	cancelQueue();

	serviceMode = false;
	preambleDuration = DCC_DEFAULT_OPERATION_MODE_PREAMBLE_DURATION;

	return true;
}

//------------------------------------------------------------------------------

bool SignalGenerator::enterServiceMode()
{
	if (!started)
		return false;

	if (!serviceModeAllowed)
		return false;

	cancelQueue();

	serviceMode = true;
	preambleDuration = DCC_DEFAULT_SERVICE_MODE_PREAMBLE_DURATION;

	return true;
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

//------------------------------------------------------------------------------
// Timer event handler
//------------------------------------------------------------------------------

void SignalGenerator::onTimerOutputMatchB()
{
	switch (currentState)
	{
		case STATE_PREAMBLE:
			preambleBitHandler();
			break;
		case STATE_BYTE_START:
			startBitHandler();
			break;
		case STATE_DATA_BYTE:
			dataByteBitHandler();
			break;
		case STATE_PACKET_END:
			packetEndBitHandler();
			break;
	}
}

//------------------------------------------------------------------------------
// State machine operations
//------------------------------------------------------------------------------

#ifdef DCC_DEBUG
#define DCC_DEBUG_PIN(PIN,STATE) \
	digitalWrite(PIN,STATE)
#else
#define DCC_DEBUG_PIN(PIN,STATE) {}
#endif

//------------------------------------------------------------------------------

void SignalGenerator::preambleBitHandler()
{
#ifdef DCC_DEBUG
	if (preambleBitCount & 0x01)
		DCC_DEBUG_PIN(DCC_DEBUG_PREAMBLE_PIN, LOW);
	else
		DCC_DEBUG_PIN(DCC_DEBUG_PREAMBLE_PIN, HIGH);
#endif

	preambleBitCount--;

	if (preambleBitCount == 0)
	{
		if (currentQueueElement == NULL)
			currentQueueElement = queue.getFirst();

		if (currentQueueElement)
		{
			//
			// When we have packets to send, use them
			//

			currentBuffer = currentQueueElement->packet;
			dataByteIndex = 0;
			dataByteCount = currentBuffer->size;
		}
		else
		{
			//
			// We have no packet, just send an IDLE one
			//

			currentBuffer = &IDLE;
			dataByteIndex = 0;
			dataByteCount = currentBuffer->size;
		}

		if (currentBuffer)
		{
			currentBuffer->busy = true;
			DumpPacket(currentBuffer->buffer, currentBuffer->size);

			timer->setOutputCompareA(bit0TotalDuration);
			timer->setOutputCompareB(bit0PulseDuration);
			currentState = STATE_BYTE_START;
			DCC_DEBUG_PIN(DCC_DEBUG_BYTE_START_PIN, HIGH);
		}
		else
		{
			timer->setOutputCompareA(bit1TotalDuration);
			timer->setOutputCompareB(bit1PulseDuration);
		}
	}
	else
	{
		timer->setOutputCompareA(bit1TotalDuration);
		timer->setOutputCompareB(bit1PulseDuration);
	}
}

//------------------------------------------------------------------------------

void SignalGenerator::startBitHandler()
{
	DCC_DEBUG_PIN(DCC_DEBUG_PREAMBLE_PIN, LOW);
	DCC_DEBUG_PIN(DCC_DEBUG_READY_PIN, LOW);
	DCC_DEBUG_PIN(DCC_DEBUG_BYTE_START_PIN, LOW);

	currentDataBitCount = 8;
	byte current = currentBuffer->buffer[dataByteIndex++];
	byte next = current << 1;
	currentDataByte = next;
	byte bit = current & 0x80;
	if (bit)
	{
		timer->setOutputCompareA(bit1TotalDuration);
		timer->setOutputCompareB(bit1PulseDuration);
	}
	else
	{
		timer->setOutputCompareA(bit0TotalDuration);
		timer->setOutputCompareB(bit0PulseDuration);
	}

	currentState = STATE_DATA_BYTE;
}

//------------------------------------------------------------------------------

void SignalGenerator::dataByteBitHandler()
{
	DCC_DEBUG_PIN(DCC_DEBUG_BYTE_START_PIN, LOW);
	DCC_DEBUG_PIN(DCC_DEBUG_DATA_1_PIN, LOW);
	DCC_DEBUG_PIN(DCC_DEBUG_DATA_0_PIN, LOW);

	currentDataBitCount--;
	if (currentDataBitCount)
	{
		byte current = currentDataByte;
		byte next = current << 1;
		currentDataByte = next;
		byte bit = current & 0x80;

		if (bit)
		{
#ifdef DCC_DEBUG
			if (currentDataBitCount & 0x01)
				DCC_DEBUG_PIN(DCC_DEBUG_DATA_1_PIN, HIGH);
#endif

			timer->setOutputCompareA(bit1TotalDuration);
			timer->setOutputCompareB(bit1PulseDuration);
		}
		else
		{
#ifdef DCC_DEBUG
			if (currentDataBitCount & 0x01)
				DCC_DEBUG_PIN(DCC_DEBUG_DATA_0_PIN, HIGH);
#endif

			timer->setOutputCompareA(bit0TotalDuration);
			timer->setOutputCompareB(bit0PulseDuration);
		}
	}
	else
	{
		dataByteCount--;
		if (dataByteCount > 0)
		{
			currentState = STATE_BYTE_START;

			timer->setOutputCompareA(bit0TotalDuration);
			timer->setOutputCompareB(bit0PulseDuration);

			DCC_DEBUG_PIN(DCC_DEBUG_DATA_BYTE_START_PIN, HIGH);

		}
		else
		{
			currentState = STATE_PACKET_END;

			timer->setOutputCompareA(bit1TotalDuration);
			timer->setOutputCompareB(bit1PulseDuration);

			DCC_DEBUG_PIN(DCC_DEBUG_PACKET_END_PIN, HIGH);
		}
	}
}

//------------------------------------------------------------------------------

void SignalGenerator::packetEndBitHandler()
{
	DCC_DEBUG_PIN(DCC_DEBUG_PACKET_END_PIN, LOW);
	DCC_DEBUG_PIN(DCC_DEBUG_PREAMBLE_PIN, HIGH);

	timer->setOutputCompareA(bit1TotalDuration);
	timer->setOutputCompareB(bit1PulseDuration);

	if (currentQueueElement)
	{
		currentQueueElement->packet->busy = false;

		PACKET_QUEUE_ELEMENT *nextElement = NULL;

		if (currentQueueElement->permanent)
			nextElement = queue.getNext(currentQueueElement);
		else
		{
			if ((currentQueueElement->transmitionCount > 1)
					&& (currentQueueElement->asAWhole))
				nextElement = currentQueueElement;
			else
				nextElement = queue.getNext(currentQueueElement);
		}

		if (!currentQueueElement->permanent)
		{
			currentQueueElement->transmitionCount--;

			if (currentQueueElement->transmitionCount == 0)
			{
				queue.release(currentQueueElement);
			}
		}

		currentQueueElement = nextElement;
	}

	//
	// After a packet end we must issue a new preamble and loop...
	//

	preambleBitCount = preambleDuration;
	currentState = STATE_PREAMBLE;
}

//------------------------------------------------------------------------------
