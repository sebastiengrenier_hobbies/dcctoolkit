/*
 * Packet.h
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 */

#ifndef PACKET_H_
#define PACKET_H_

#include "Arduino.h"

#include "Constants.h"

#define PACKET_BUFFER_MAX_SIZE	6
#define PACKET_BUFFER_PAGE_SIZE 16

typedef struct
{
	uint16_t address;
	byte buffer[PACKET_BUFFER_MAX_SIZE];
	byte size;
	bool busy;
} PACKET_BUFFER;

typedef struct
{
	PACKET_BUFFER *cache;
	void *next;
} PACKET_BUFFER_PAGE;

class Packet
{
	public:

	static void healthCheck();

	static void release(PACKET_BUFFER *packet);

	//
	// BaselinePacket related operations
	//

	static PACKET_BUFFER* reset();
	static PACKET_BUFFER* idle();

	static PACKET_BUFFER* speed28StepAndDirection(uint8_t shortAddress,
			uint8_t speed, bool forward);

	//
	// ExtendedPacket related operations
	//

	static PACKET_BUFFER* speed128StepAndDirection(uint16_t longAddress,
			uint8_t speed, bool forward);

	static PACKET_BUFFER* functionGroup(uint16_t longAddress,
			FUNCTION_GROUPS group, uint8_t mask);

	//
	// ServiceModePacket related operations

	static PACKET_BUFFER* directVerifyBit(uint16_t cv, uint8_t bitIndex,
			bool value);

	static PACKET_BUFFER* directWriteBit(uint16_t cv, uint8_t bitIndex,
			bool value);

	static PACKET_BUFFER* directVerifyByte(uint16_t cv, uint8_t value);

	static PACKET_BUFFER* directWriteByte(uint16_t cv, uint8_t value);

	//
	// Internal operations
	//

	protected:

	static PACKET_BUFFER_PAGE* allocatePage();

	static PACKET_BUFFER* allocate(uint8_t size);

	static PACKET_BUFFER* baseline(uint8_t shortAddress, uint8_t dataByte);

	static PACKET_BUFFER* extended(uint16_t longAddress, uint8_t *data,
			uint8_t dataLength);

	static PACKET_BUFFER* extended(uint16_t longAddress, COMMAND_GROUP command,
			uint8_t dataByte1);

	static PACKET_BUFFER* extended(uint16_t longAddress, COMMAND_GROUP command,
			uint8_t dataByte1, uint8_t dataByte2);

	static PACKET_BUFFER* extended(uint16_t longAddress, COMMAND_GROUP command,
			uint8_t dataByte1, uint8_t dataByte2, uint8_t dataByte3);

	static PACKET_BUFFER* extended(uint16_t longAddress, COMMAND_GROUP command,
			uint8_t dataByte1, uint8_t dataByte2, uint8_t dataByte3,
			uint8_t dataByte4);

	static PACKET_BUFFER* serviceMode(CV_BIT_MANIPULATION_OPERATION operation,
			uint16_t cvNumber, uint8_t bitNumber, bool value);

	static PACKET_BUFFER* serviceMode(CV_ACCESS_SUBCOMMAND subCommand,
			uint16_t cvNumber, uint8_t value);
};

#endif /* PACKET_H_ */
