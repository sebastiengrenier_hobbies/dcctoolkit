/*
 * DccToolkitConfig.h
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 * Use this configuration file to setup the DccToolkitLibrary.
 *
 * You may also use one of the configuration sample provided in the sampleconfig
 * directory.
 *
 */

#ifndef DCCTOOLKITCONFIG_H_
#define DCCTOOLKITCONFIG_H_

//
// Defining DCC_DEBUG_QUEUE will produce lot of debugging messages about
// queue management on serial console
//

//#define DCC_DEBUG_QUEUE

//
// Defining DCC_DEBUG_ACK will produce lot of debugging messages about
// signal ack timing and measure
//

//#define DCC_DEBUG_ACK

//
// Defining DCC_DEBUG_SIGNAL while switch the library to a non-functional code.
// Instead of generating proper DCC signal, it will make several leds bling
// on several ports for code debugging purpose !
//
//#define DCC_DEBUG_SIGNAL

#ifdef DCC_DEBUG
#define	DCC_DEBUG_PREAMBLE_PIN		22
#define	DCC_DEBUG_READY_PIN			24
#define	DCC_DEBUG_BYTE_START_PIN	26
#define DCC_DEBUG_DATA_0_PIN		28
#define	DCC_DEBUG_DATA_1_PIN		30
#define	DCC_DEBUG_PACKET_END_PIN	32
#endif

#endif /* DCCTOOLKITCONFIG_H_ */
