/*
 * Packet.cpp
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 */

#include "Packet.h"

//==============================================================================
//
// Packet heap cache management, used to avoir real heap framentation
//
//==============================================================================

static PACKET_BUFFER_PAGE *FIRST_PAGE = NULL;

//==============================================================================
//
// Public operations
//
//==============================================================================

void Packet::healthCheck()
{
	int pageIndex = 1;

	PACKET_BUFFER_PAGE *page = FIRST_PAGE;

	while (page)
	{
		Serial.print("Page ");
		Serial.println(pageIndex);

		for (int i = 0; i < PACKET_BUFFER_PAGE_SIZE; i++)
		{
			Serial.print("Packet ");
			Serial.print(i);
			Serial.print(" address ");
			Serial.print(page->cache[i].address);
			Serial.print(" buffer ");
			Serial.print((int) page->cache[i].buffer);
			Serial.print(" busy ");
			Serial.print(page->cache[i].busy);
			Serial.print(" size ");
			Serial.print(page->cache[i].size);
			Serial.println("");
		}

		pageIndex++;
		page = (PACKET_BUFFER_PAGE*) page->next;
	}

	Serial.println("Done healthcheck");
}

//------------------------------------------------------------------------------
// BaselinePacket related operations
//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::reset()
{
	return baseline(DCC_SHORT_ADDRESS_BROADCAST, 0);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::idle()
{
	return baseline(DCC_SHORT_ADDRESS_RESERVED_IDLE, 0);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::speed28StepAndDirection(uint8_t shortAddress,
		uint8_t speed, bool forward)
{
	byte speedBits = speed & 0b00011111;

	byte dataByte = 0b01000000;
	dataByte |= ((speedBits & 0b00001) << 4);
	dataByte |= ((speedBits & 0b11110) >> 1);
	dataByte |= (forward ? 0b00100000 : 0);

	return baseline(shortAddress, dataByte);
}

//------------------------------------------------------------------------------
// ExtendedPacket related operations
//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::speed128StepAndDirection(uint16_t longAddress,
		uint8_t speed, bool forward)
{
	return extended(longAddress, COMMAND_GROUP_ADVANCED, 0x1F,
			speed | (forward ? 0x80 : 0));
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::functionGroup(uint16_t longAddress,
		FUNCTION_GROUPS group, uint8_t mask)
{
	switch (group)
	{
		case FUNCTIONS_GROUP_00_04:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_00_04,
					mask & 0x1F);
		case FUNCTIONS_GROUP_05_08:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_05_12,
					(mask & 0x0F)
							| COMMAND_GROUP_FUNCTIONS_05_12_SUBCOMMAND_05_08);
		case FUNCTIONS_GROUP_09_12:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_05_12,
					(mask & 0x0F)
							| COMMAND_GROUP_FUNCTIONS_05_12_SUBCOMMAND_09_12);
		case FUNCTIONS_GROUP_13_16:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_13_28,
					COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_13_20,
					(mask & 0x0F));
		case FUNCTIONS_GROUP_17_20:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_13_28,
					COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_13_20,
					(mask & 0x0F) | 0x80);
		case FUNCTIONS_GROUP_21_24:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_13_28,
					COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_21_28,
					(mask & 0x0F));
		case FUNCTIONS_GROUP_25_28:
			return extended(longAddress, COMMAND_GROUP_FUNCTIONS_13_28,
					COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_21_28,
					(mask & 0x0F) | 0x80);
	}

	return NULL;
}

//------------------------------------------------------------------------------
// ServiceMode related operations
//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::directVerifyBit(uint16_t cv, uint8_t bitIndex,
		bool value)
{
	return serviceMode(CV_BIT_MANIPULATION_OPERATION_VERIFY, cv, bitIndex,
			value);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::directWriteBit(uint16_t cv, uint8_t bitIndex, bool value)
{
	return serviceMode(CV_BIT_MANIPULATION_OPERATION_WRITE, cv, bitIndex, value);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::directVerifyByte(uint16_t cv, uint8_t value)
{
	return serviceMode(CV_ACCESS_SUBCOMMAND_VERIFY_BYTE, cv, value);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::directWriteByte(uint16_t cv, uint8_t value)
{
	return serviceMode(CV_ACCESS_SUBCOMMAND_WRITE_BYTE, cv, value);
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

void Packet::release(PACKET_BUFFER *packet)
{
	packet->address = 0;
	packet->size = 0;
	packet->busy = false;
}

//------------------------------------------------------------------------------

PACKET_BUFFER_PAGE* Packet::allocatePage()
{
	PACKET_BUFFER_PAGE *page = new PACKET_BUFFER_PAGE;
	page->next = NULL;
	page->cache = new PACKET_BUFFER[PACKET_BUFFER_PAGE_SIZE];

	for (uint8_t i = 0; i < PACKET_BUFFER_PAGE_SIZE; i++)
	{
		page->cache[i].address = 0;
		page->cache[i].size = 0;
		page->cache[i].busy = false;
		memset(page->cache[i].buffer, 0, PACKET_BUFFER_MAX_SIZE);
		typedef struct
		{
			PACKET_BUFFER *cache;
			void *next;
		} PACKET_BUFFER_PAGE;
	}

	return page;
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::allocate(uint8_t size)
{
	if (FIRST_PAGE == NULL)
		FIRST_PAGE = allocatePage();

	PACKET_BUFFER_PAGE *currentPage = FIRST_PAGE;

	while (currentPage != NULL)
	{
		for (uint8_t i = 0; i < PACKET_BUFFER_PAGE_SIZE; i++)
		{
			if (currentPage->cache[i].size == 0)
			{
				if (!currentPage->cache[i].busy)
				{
					currentPage->cache[i].size = size;
					return &(currentPage->cache[i]);
				}
			}
		}

		currentPage = (PACKET_BUFFER_PAGE*) currentPage->next;
	}

	currentPage = allocatePage();
	currentPage->next = FIRST_PAGE;
	FIRST_PAGE = currentPage;

	currentPage->cache[0].size = size;
	return &(currentPage->cache[0]);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::baseline(uint8_t shortAddress, uint8_t dataByte)
{
	PACKET_BUFFER *result = allocate(3);

	result->address = shortAddress;

	result->size = 3;

	result->buffer[0] = shortAddress;
	byte checksum = result->buffer[0];

	result->buffer[1] = dataByte;
	checksum ^= dataByte;

	result->buffer[2] = checksum;

	return result;
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::extended(uint16_t longAddress, uint8_t *data,
		uint8_t dataLength)
{
	PACKET_BUFFER *result = allocate(dataLength + 3);

	result->address = longAddress;
	int index = 0;

	//
	// Store the address in appropriate format
	//

	if (longAddress > DCC_SHORT_ADDRESS_MASK)
	{
		result->size = 2 + dataLength + 1;
		result->buffer[0] = longAddress >> 8 | DCC_LONG_ADDRESS_MASK;
		result->buffer[1] = longAddress & 0xFF;
		index = 2;
	}
	else
	{
		result->size = 1 + dataLength + 1;
		result->buffer[0] = (uint8_t) (longAddress & DCC_SHORT_ADDRESS_MASK);
		index = 1;
	}

	//
	// Append the data
	//

	memcpy(&result->buffer[index], data, dataLength);
	index += dataLength;

	//
	// Compute the checksum as XOR of all buffer's bytes and append it
	//

	byte checksum = result->buffer[0];
	for (uint8_t i = 1; i < index; i++)
		checksum ^= result->buffer[i];

	result->buffer[index] = checksum;

	return result;
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::extended(uint16_t longAddress, COMMAND_GROUP command,
		uint8_t dataByte1)
{
	uint8_t data[1];
	data[0] = ((command & 0x7) << 5) | (dataByte1 & COMMAND_ARGUMENT_MASK);

	return extended(longAddress, data, 1);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::extended(uint16_t longAddress, COMMAND_GROUP command,
		uint8_t dataByte1, uint8_t dataByte2)
{
	uint8_t data[2];

	data[0] = ((command & 0x7) << 5) | (dataByte1 & COMMAND_ARGUMENT_MASK);
	data[1] = dataByte2;

	return extended(longAddress, data, 2);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::extended(uint16_t longAddress, COMMAND_GROUP command,
		uint8_t dataByte1, uint8_t dataByte2, uint8_t dataByte3)
{
	uint8_t data[3];

	data[0] = ((command & 0x7) << 5) | (dataByte1 & COMMAND_ARGUMENT_MASK);
	data[1] = dataByte2;
	data[2] = dataByte3;

	return extended(longAddress, data, 3);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::extended(uint16_t longAddress, COMMAND_GROUP command,
		uint8_t dataByte1, uint8_t dataByte2, uint8_t dataByte3,
		uint8_t dataByte4)
{
	uint8_t data[4];

	data[0] = ((command & 0x7) << 5) | (dataByte1 & COMMAND_ARGUMENT_MASK);
	data[1] = dataByte2;
	data[2] = dataByte3;
	data[3] = dataByte4;

	return extended(longAddress, data, 4);
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::serviceMode(CV_BIT_MANIPULATION_OPERATION operation,
		uint16_t cvNumber, uint8_t bitNumber, bool value)
{
	PACKET_BUFFER *result = allocate(4);

	//
	// CV are transmitted from 0 to 1023 whereas they are numbered 1 to 1024
	//

	cvNumber--;

	result->address = 0;
	result->size = 4;

	result->buffer[0] = COMMAND_GROUP_CV_ACCESS << 4;
	result->buffer[0] |= (CV_ACCESS_SUBCOMMAND_BIT_MANIPULATION << 2);
	result->buffer[0] |= ((cvNumber >> 8) & 0x03);

	result->buffer[1] = cvNumber & 0xFF;

	result->buffer[2] = (0b111 << 5) | (operation << 4);
	result->buffer[2] |= (value ? 0b1000 : 0) | (bitNumber & 0x07);

	result->buffer[3] = result->buffer[0] ^ result->buffer[1]
			^ result->buffer[2];

	return result;
}

//------------------------------------------------------------------------------

PACKET_BUFFER* Packet::serviceMode(CV_ACCESS_SUBCOMMAND subCommand,
		uint16_t cvNumber, uint8_t value)
{
	PACKET_BUFFER *result = allocate(4);

	//
	// CV are transmitted from 0 to 1023 whereas they are numbered 1 to 1024
	//

	cvNumber--;

	result->address = 0;
	result->size = 4;

	result->buffer[0] = COMMAND_GROUP_CV_ACCESS << 4;
	result->buffer[0] |= (subCommand << 2);
	result->buffer[0] |= ((cvNumber >> 8) & 0x03);

	result->buffer[1] = cvNumber & 0xFF;

	result->buffer[2] = value;

	result->buffer[3] = result->buffer[0] ^ result->buffer[1]
			^ result->buffer[2];

	return result;
}

//------------------------------------------------------------------------------
