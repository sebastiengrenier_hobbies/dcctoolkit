## **DccToolkit Library V1.0** for Arduino

**Written by:** _Sébastien GRENIER_

### **Acknowledgements**

I would like to thanks the following people we brought valuable contribution to the Open Source scene and offered famous projects from which this library may be partialy inspired :

* Gregg E. Berman who wrote the original DCC++. Visit https://github.com/DccPlusPlus for sources and https://sites.google.com/site/dccppsite/home for more information ;


* Thierry PARIS who fixed some part of the original software and made a library version of it called DCCpp. Visit https://www.locoduino.org for more information ;

and on a global perspective, all the people participating in projects helping all modelers accross the world to use DCC in a convenient way, with great features at low cost ! 

### **What is the DccToolkit library ?**

Modern model railway make use of digital components to drive locomotives and accessories around de layout.
All these devices are driven through a standard digital protocol defined by the NMRA (National Model Railway Association / USA). General information about this standard can be found here : https://www.nmra.org/dcc-working-group

DccToolkit allows to build DCC compliant equipments using the Arduino hardware & software.

The library provide two main classes :
* A signal generator class which, together with packet definition classes, can be used to drive a model track and send commands to slave devices connected to the track.
* A signal decoder class used to build device's internal decoding logic, either for mobile equipments such as locomotives or coaches, or for accessories such as turnouts and signals.

Technical information about the DCC protocol can be found here : https://www.nmra.org/sites/default/files/s-92-2004-07.pdf

### **How to use it**

First, you have to import the SystemToolkit library into your project as the DccToolkit make use of it.

This library can be found here : https://gitlab.com/sebastiengrenier_hobbies/systemtoolkit

Add this path in the library manager of your Arduino IDE then import the SystemToolkit library into your project.

Second, you have to setup the library by providing a configuration file called _DccToolkitConfig.h_ and another one called  _SystemToolkitConfig.h_ for the corresponding library. Be sure to enable the required timer you wish to use for signal generation !

These files should be present in the root path of your project. You can either write your own files or take one sample file from the sampleconfig directories of either libraries, then adapt them to suit your needs.

Most of the time, you won't have to change the content of this file but this may occur in rare situations, especially for debug purpose.

#SystemToolkitConfig.h#

```Arduino
#ifndef SYSTEMTOOLKITCONFIG_H_
#define SYSTEMTOOLKITCONFIG_H_

#define SYSTEM_TOOLKIT_ENABLE_TIMER_0
#define SYSTEM_TOOLKIT_ENABLE_TIMER_1
#define SYSTEM_TOOLKIT_ENABLE_TIMER_2

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define SYSTEM_TOOLKIT_ENABLE_TIMER_3
#define SYSTEM_TOOLKIT_ENABLE_TIMER_4
#define SYSTEM_TOOLKIT_ENABLE_TIMER_5
#endif

#endif
```

#DccToolkitConfig.h#
```Arduino
#ifndef DCCTOOLKITCONFIG_H_
#define DCCTOOLKITCONFIG_H_

//
// Defining DCC_DEBUG while switch the library to a non-functional code.
// Instead of generating proper DCC signal, it will make several leds bling
// on several ports for code debugging purpose !
//
//#define DCC_DEBUG

#ifdef DCC_DEBUG
#define	DCC_DEBUG_PREAMBLE_PIN		22
#define	DCC_DEBUG_READY_PIN			24
#define	DCC_DEBUG_BYTE_START_PIN	26
#define DCC_DEBUG_DATA_0_PIN		28
#define	DCC_DEBUG_DATA_1_PIN		30
#define	DCC_DEBUG_PACKET_END_PIN	32
#endif

#endif
```

Then all you have to do is to use the library's classes to build the system you target.

This short code snipset show how to start a signal generator and send a reset packet through the corresponding pin 12, which on an Arduino MEGA hold the OUTPUT COMPARE B output of timer 1. Pin 11 is also used to provide a constant high signal. PIN 11 can be connected to the PWM input of a motor shield. Pin 12 shall be connected to the DIRECTION input of the same shield.

```Arduino
#include <DccToolkit.h>

SignalGenerator generator(1,false);

void setup(){
	pinMode(11,OUTPUT);
	pinMode(12,OUTPUT);
	digitalWrite(11,HIGH);
	generator.start();
}

void loop(){
	generator.sendReset();
}

```

### **Classes**

There are two low-level classes that you may use to build your project : SignalGenerator is used to build a base-station, whereas SignalDecoder is used to build a decoder. Other classes are either technical aspect used by these two classes or higher-level implementation of specialized systems.

#### **SignalGenerator**

Use this class to provide a DCC signal on one of the PWM output pin of your Arduino. SignalGenerator use a Timer from the SystemToolkit library, this the corresponding timer number must be enabled in the corresponding configuration. Timer should be a high resolution (16 bits) timer but low resolution (8 bits) are also supported, with a lower signal accuracy regarding the specification. 

SignalGenerator setup the timer in FAST PWM signal generation with no operation on COMPARE A and TOGGLE operation on COMPARE B. Thus it is OUTPUT COMPARE B pin which provide the pulsed signal to feed power stage of your device.

The most common design consist in feeding the PWM signal to the DIRECTION input pin of a motor hardware driver. The normal PWM input pin is not used for pulse, but to turn ON/OFF the whole signal. As COMPARE A is not used at timer level, I recommand using OUTPUT COMPARE A regular pin to switch motor driver ON/OFF when required, so that no ressource is lost on the Arduino I/O. Please note that SignalGenerator does not setup any output pin but the OUTPUT COMPARE B of its associated timer. It's up to you to take care of this part.

Once started, SignalGenerator will act on two mode depending on the serviceModeAllowed flag provided in constructor.

When ServiceMode is allowed, programming operations are allowed on the track and `enterServiceMode()`calls will be accepted. When ServiceMode is not allowed, only calls to `enterOperationMode()` and related Operation functions will be accepted.

OperationMode is the normal mode of a SignalGenerator : this is the one (in respect with the NMRA specification) where train control operations occurs.

When in operation mode, your code can call `queuePacket()` or `sendPacket()` to send corresponding commands to the track. `sendPacket()` is used to send packets i na synchroneous way. This means that the function will only return when packets are actually sent. On the contrary, ̀`queuePacket()` is used to submit packet through a waiting queue. SignalGenerator will handle all submitted packets using a best effort strategy.

A third function called `queuePermanentPacket()` is used to submit packets that should be transmitted in a continuous way, whereas the two other functions will ask you to provide a number of transmission occurence (usually, a packet is never transmitted only once, as the target decoder may not receive it in any situation. For example, a moving locomotive may be shortly disconnected from the track when crossing a turnout, thus not being able to receive a command).

Train control (speed & direction) commands are the ones that you shall transmit through as permanent packets, whereas function control or accessory commands would usualy be transmitted somewhat between 5 or 10 times, not more.