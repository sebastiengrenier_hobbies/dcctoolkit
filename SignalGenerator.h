/*
 * SignalGenerator.h
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 */

#include <Arduino.h>

#ifndef SignalGenerator_H_
#define SignalGenerator_H_

#include <SystemToolkit.h>

#include "DccToolkitConfig.h"

#include "Packet.h"
#include "PacketQueue.h"

//==============================================================================
//
// Internal state machine states definition
//
//==============================================================================

typedef enum
{
	STATE_PREAMBLE, STATE_BYTE_START, STATE_DATA_BYTE, STATE_PACKET_END
} SIGNAL_GENERATION_STATE;

//==============================================================================
//
// Construction / Destruction
//
//==============================================================================

class SignalGenerator: public TimerEventHandler
{
	private:

	byte timerIndex;
	Timer *timer;

	uint8_t signalPinNumber;

	bool started;

	bool serviceModeAllowed;
	bool serviceMode;

	uint16_t bit0PulseDuration;
	uint16_t bit0TotalDuration;
	uint16_t bit1PulseDuration;
	uint16_t bit1TotalDuration;

	uint8_t preambleDuration;
	uint8_t preambleBitCount;

	PacketQueue queue;
	PACKET_QUEUE_ELEMENT *currentQueueElement;

	PACKET_BUFFER *currentBuffer;
	uint8_t dataByteCount;
	uint8_t dataByteIndex;
	uint8_t currentDataByte;
	uint8_t currentDataBitCount;

	SIGNAL_GENERATION_STATE currentState;

	public:

	SignalGenerator(uint8_t aTimerIndex,uint8_t aPinNumber, bool aServiceModeAllowed);
	virtual ~SignalGenerator();

	bool isServiceModeAllowed()
	{
		return serviceModeAllowed;
	}

	bool isServiceMode()
	{
		return serviceMode;
	}

	bool isStarted()
	{
		return started;
	}

	void start();
	void stop();

	uint32_t packetDuration(PACKET_BUFFER *aPacket);

	void lockQueue();

	void unlockQueue();

	void cancelQueue();

	void queuePermanentPacket(PACKET_BUFFER *aPacket);

	void cancelAnyPermanentPacket(uint16_t address);

	void queuePacket(PACKET_BUFFER *aPacket, uint8_t count);

	void sendPacket(PACKET_BUFFER *aPacket, uint8_t count);

	bool enterOperationMode();

	bool enterServiceMode();

	//
	// Timer event handler
	//

	void onTimerOutputMatchB();

	private:

	//
	// Internal state machine handlers called on each timer event, depending
	// on current state...(currentHandler)
	//

	void preambleBitHandler();
	void startBitHandler();
	void dataByteBitHandler();
	void packetEndBitHandler();
};

#endif /* SignalGenerator_H_ */
