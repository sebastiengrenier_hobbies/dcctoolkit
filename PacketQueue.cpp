/*
 * PacketQueue.cpp
 *
 *  Created on: 13 mars 2021
 *      Author: Sébastien GRENIER
 */

#include "PacketQueue.h"
#include "DccToolkitConfig.h"

//==============================================================================
//
// Construction / destruction
//
//==============================================================================

PacketQueue::PacketQueue()
{
	firstPage = allocatePage();
	firstElement = NULL;
	lastElement = NULL;
	lockedFirstElement = NULL;
	locked = false;
}

//------------------------------------------------------------------------------

PacketQueue::~PacketQueue()
{
}

//==============================================================================
//
// Public operations
//
//==============================================================================

void PacketQueue::release(PACKET_QUEUE_ELEMENT *element)
{
#ifdef DCC_DEBUG_QUEUE
		Serial.print(" RELEASE ");
#endif

	PACKET_QUEUE_ELEMENT *previous = (PACKET_QUEUE_ELEMENT*) element->previous;
	PACKET_QUEUE_ELEMENT *next = (PACKET_QUEUE_ELEMENT*) element->next;

	Packet::release(element->packet);

	if (element == firstElement)
		firstElement = next;

	if (element == lastElement)
		lastElement = previous;

	if (previous)
		previous->next = next;
	if (next)
		next->previous = previous;

	element->permanent = false;
	element->asAWhole = false;
	element->transmitionCount = 0;

	element->previous = NULL;
	element->next = NULL;

	//
	// Always remove packet last as it is our in use marker !
	//

	element->packet = NULL;
}

//------------------------------------------------------------------------------

bool PacketQueue::isEmpty()
{
	return firstElement == NULL;
}

//------------------------------------------------------------------------------

void PacketQueue::clearAll()
{
	PACKET_QUEUE_ELEMENT *element = getFirst();

	while (element != NULL)
	{
		PACKET_QUEUE_ELEMENT *next = getNext(element);

		release(element);

		element = next;

#ifdef DCC_DEBUG_QUEUE
		Serial.print(" CLR ");
#endif
	}

	locked = false;
	firstElement = NULL;
	lastElement = NULL;
	lockedFirstElement = NULL;
}

//------------------------------------------------------------------------------

void PacketQueue::clearForAddress(uint16_t address, bool permanent)
{
	PACKET_QUEUE_ELEMENT *element = getFirst();
	while (element != NULL)
	{
		PACKET_QUEUE_ELEMENT *next = getNext(element);

		if (element->packet->address == address)
		{
			if (permanent == element->permanent)
				release(element);
		}

		element = next;

#ifdef DCC_DEBUG_QUEUE
		Serial.print(" CLR ");
#endif
	}
}

//------------------------------------------------------------------------------

void PacketQueue::lock()
{
	if (locked)
		return;

	locked = true;
}

//------------------------------------------------------------------------------

void PacketQueue::unlock()
{
	if (!locked)
		return;

	firstElement = lockedFirstElement;

	lockedFirstElement = NULL;
	locked = false;
}

//------------------------------------------------------------------------------

void PacketQueue::queue(PACKET_BUFFER *buffer, int count, bool permanent,
		bool asAWhole)
{
	if ((locked) && (permanent))
		return;

	PACKET_QUEUE_ELEMENT *element = allocate(buffer, count, permanent, asAWhole);

	if (lastElement)
	{
		lastElement->next = element;
		element->previous = lastElement;
	}

	lastElement = element;

	if (locked)
	{
		if (lockedFirstElement == NULL)
			lockedFirstElement = lastElement;
	}
	else
	{
		if (firstElement == NULL)
			firstElement = element;
	}
}

//------------------------------------------------------------------------------

PACKET_QUEUE_ELEMENT* PacketQueue::getFirst()
{
	return firstElement;
}

//------------------------------------------------------------------------------

PACKET_QUEUE_ELEMENT* PacketQueue::getNext(PACKET_QUEUE_ELEMENT *current)
{
	if (current == NULL)
		return NULL;

	return (PACKET_QUEUE_ELEMENT*) (current->next);
}

//==============================================================================
//
// Internal operations
//
//==============================================================================

PACKET_QUEUE_PAGE* PacketQueue::allocatePage()
{
	PACKET_QUEUE_PAGE *page = new PACKET_QUEUE_PAGE;
	page->next = NULL;
	page->cache = new PACKET_QUEUE_ELEMENT[PACKET_QUEUE_PAGE_SIZE];

	for (uint8_t i = 0; i < PACKET_QUEUE_PAGE_SIZE; i++)
	{
		page->cache[i].packet = NULL;
		page->cache[i].transmitionCount = 0;
		page->cache[i].permanent = false;
		page->cache[i].asAWhole = false;
		page->cache[i].previous = NULL;
		page->cache[i].next = NULL;
	}

	return page;
}

//------------------------------------------------------------------------------

PACKET_QUEUE_ELEMENT* PacketQueue::allocate(PACKET_BUFFER *buffer, int count,
		bool permanent,bool asAWhole)
{
	PACKET_QUEUE_PAGE *currentPage = firstPage;

	while (currentPage != NULL)
	{
		for (uint8_t i = 0; i < PACKET_BUFFER_PAGE_SIZE; i++)
		{
			if (currentPage->cache[i].packet == NULL)
			{
				currentPage->cache[i].packet = buffer;
				currentPage->cache[i].permanent = permanent;
				currentPage->cache[i].transmitionCount = count;
				currentPage->cache[i].asAWhole = asAWhole;

				return &(currentPage->cache[i]);
			}
		}

		currentPage = (PACKET_QUEUE_PAGE*) currentPage->next;
	}

	currentPage = allocatePage();
	currentPage->next = firstPage;
	firstPage = currentPage;

	currentPage->cache[0].packet = buffer;
	currentPage->cache[0].permanent = permanent;
	currentPage->cache[0].asAWhole = asAWhole;
	currentPage->cache[0].transmitionCount = count;

	return &(currentPage->cache[0]);
}

//------------------------------------------------------------------------------

