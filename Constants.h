/*
 * Constants.h
 *
 *  Created on: 19 févr. 2021
 *      Author: Sébastien GRENIER
 *
 * General purpose constants used across the library.
 *
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include "Arduino.h"

//==============================================================================
//
// Here are the 0 and 1 bit duration given in timer tick count for both high &
// low resolution timers, assuming a 16 Mhz CPU frequency
//
//==============================================================================

//------------------------------------------------------------------------------
// Signal generation constants
//------------------------------------------------------------------------------

#define DCC_ONE_BIT_TOTAL_DURATION_MICROSECONDS 	116
#define DCC_ZERO_BIT_TOTAL_DURATION_MICROSECONDS	200

#define DCC_ONE_BIT_SCAN_TOTAL_DURATION_MICROSECOND		128
#define DCC_ZERO_BIT_SCAN_TOTAL_DURATION_MICROSECOND	20000

//==============================================================================
// Timer steps converted durations, with 16bit prescaled to 1 and
// 8 bit prescaled to 64, so that each values fit into the timer capabilities
// Special case for timer 2 as it can prescale to 32, which is not possible for
// timer 0 which allows only 8 or 64 but nothing in between.
//==============================================================================

//------------------------------------------------------------------------------
// Timing for 8 Mhz slow ARDUINO (no crystal)
//------------------------------------------------------------------------------

#if (F_CPU==8000000L)

//
// 8 bit timer 0 with prescale = 64
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_0 24
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_0 12

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_0 14
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_0 7

//
// 8 bit timer 2 with prescale = 32
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_2 50
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_2 25

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_2 29
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_2 14

//
// 16 bit timers with prescale = 1
//

#define DCC_ZERO_BIT_TOTAL_DURATION_16_BIT_TIMER 1599
#define DCC_ZERO_BIT_PULSE_DURATION_16_BIT_TIMER 799

#define DCC_ONE_BIT_TOTAL_DURATION_16_BIT_TIMER 927
#define DCC_ONE_BIT_PULSE_DURATION_16_BIT_TIMER 463


//------------------------------------------------------------------------------
// Timing for 16 Mhz standard ARDUINO
//------------------------------------------------------------------------------

#elif (F_CPU==16000000L)

//
// 8 bit timer 0 with prescale = 64
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_0 48
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_0 24

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_0 28
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_0 14

//
// 8 bit timer 2 with prescale = 32
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_2 100
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_2 50

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_2 58
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_2 29

//
// 16 bit timers with prescale = 1
//

#define DCC_ZERO_BIT_TOTAL_DURATION_16_BIT_TIMER 3199
#define DCC_ZERO_BIT_PULSE_DURATION_16_BIT_TIMER 1599

#define DCC_ONE_BIT_TOTAL_DURATION_16_BIT_TIMER 1855
#define DCC_ONE_BIT_PULSE_DURATION_16_BIT_TIMER 927

//------------------------------------------------------------------------------
// Timing for 20 Mhz boosted ARDUINO clones
//------------------------------------------------------------------------------

#elif (F_CPU==20000000L)

//
// 8 bit timer 0 with prescale = 64
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_0 62
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_0 31

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_0 36
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_0 18

//
// 8 bit timer 2 with prescale = 32
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_2 124
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_2 62

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_2 72
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_2 36

//
// 16 bit timers with prescale = 1
//

#define DCC_ZERO_BIT_TOTAL_DURATION_16_BIT_TIMER 4000
#define DCC_ZERO_BIT_PULSE_DURATION_16_BIT_TIMER 2000

#define DCC_ONE_BIT_TOTAL_DURATION_16_BIT_TIMER 2320
#define DCC_ONE_BIT_PULSE_DURATION_16_BIT_TIMER 1160

//------------------------------------------------------------------------------
// Timing for 32 Mhz standard ARDUINO
//------------------------------------------------------------------------------

#elif (F_CPU==32000000L)

//
// 8 bit timer 0 with prescale = 64
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_0 96
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_0 48

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_0 56
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_0 28

//
// 8 bit timer 2 with prescale = 32
//

#define DCC_ZERO_BIT_TOTAL_DURATION_8_BIT_TIMER_2 200
#define DCC_ZERO_BIT_PULSE_DURATION_8_BIT_TIMER_2 100

#define DCC_ONE_BIT_TOTAL_DURATION_8_BIT_TIMER_2 116
#define DCC_ONE_BIT_PULSE_DURATION_8_BIT_TIMER_2 58

//
// 16 bit timers with prescale = 1
//

#define DCC_ZERO_BIT_TOTAL_DURATION_16_BIT_TIMER 6398
#define DCC_ZERO_BIT_PULSE_DURATION_16_BIT_TIMER 3199

#define DCC_ONE_BIT_TOTAL_DURATION_16_BIT_TIMER 3710
#define DCC_ONE_BIT_PULSE_DURATION_16_BIT_TIMER 1855

//------------------------------------------------------------------------------
// Unsupported frequency !
//------------------------------------------------------------------------------
#else

#error Unsupported CPU frequency. Please provide appropriate timings.

#endif

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#define DCC_MINIMUM_OPERATION_MODE_PREAMBLE_DURATION		14
#define DCC_DEFAULT_OPERATION_MODE_PREAMBLE_DURATION		16

#define DCC_MINIMUM_SERVICE_MODE_PREAMBLE_DURATION			20
#define DCC_DEFAULT_SERVICE_MODE_PREAMBLE_DURATION			24

#define DCC_

#define DCC_BASIC_ACK_SAMPLES_PER_POINT						50
#define DCC_BASIC_ACK_SAMPLES_PER_CHECK						800

#define DCC_BASIC_ACK_DURATION_MILLISEC						6
#define DCC_BASIC_ACK_DURATION_MILLISEC_MIN					5
#define DCC_BASIC_ACK_DURATION_MILLISEC_MAX					7

#define DCC_BASIC_ACK_CURRENT_LOAD_MILLIAMP					40

//
// Address related constants
//

#define DCC_SHORT_ADDRESS_MASK								0x7F
#define DCC_LONG_ADDRESS_MASK								0xC0

//
// Primary (single byte) address partitions
//

#define DCC_SHORT_ADDRESS_BROADCAST							0x00000000

#define DCC_SHORT_ADDRESS_MIN								0b00000001
#define DCC_SHORT_ADDRESS_MAX								0b01111111

#define DCC_MIDDLE_ADDRESS_ACCESSORY_MIN					0b10000000
#define DCC_MIDDLE_ADDRESS_ACCESSORY_MAX					0b10111111

#define DCC_LONG_ADDRESS_MIN								0b11000000
#define DCC_LONG_ADDRESS_MAX								0b11100111
#define DCC_LONG_ADDRESS_RESERVED_MIN						0b11101000
#define DCC_LONG_ADDRESS_RESERVED_MAX						0b11111110

#define DCC_SHORT_ADDRESS_RESERVED_IDLE						0b11111111

#define DCC_SHORT_ADDRESS_DEFAULT							3
#define DCC_LONG_ADDRESS_DEFAULT							3333

//
// ExtendedPacket standard commands
//

typedef enum
{
	COMMAND_GROUP_DECODER_AND_CONSIST = 0b000,
	COMMAND_GROUP_ADVANCED = 0b001,
	COMMAND_GROUP_SPEED_REVERSE = 0b010,
	COMMAND_GROUP_SPEED_FORWARD = 0b011,
	COMMAND_GROUP_FUNCTIONS_00_04 = 0b100,
	COMMAND_GROUP_FUNCTIONS_05_12 = 0b101,
	COMMAND_GROUP_FUNCTIONS_13_28 = 0b110,
	COMMAND_GROUP_CV_ACCESS = 0b111
} COMMAND_GROUP;

#define COMMAND_ARGUMENT_MASK								0b00011111

typedef enum
{
	COMMAND_GROUP_FUNCTIONS_05_12_SUBCOMMAND_05_08 = 0b00000,
	COMMAND_GROUP_FUNCTIONS_05_12_SUBCOMMAND_09_12 = 0b10000,
	COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_13_20 = 0b11110,
	COMMAND_GROUP_FUNCTIONS_13_28_SUBCOMMAND_21_28 = 0b11111
} FUNCTIONS_SUBCOMMAND;

typedef enum
{
	FUNCTIONS_GROUP_00_04 = 1,
	FUNCTIONS_GROUP_05_08 = 2,
	FUNCTIONS_GROUP_09_12 = 3,
	FUNCTIONS_GROUP_13_16 = 4,
	FUNCTIONS_GROUP_17_20 = 5,
	FUNCTIONS_GROUP_21_24 = 6,
	FUNCTIONS_GROUP_25_28 = 7
} FUNCTION_GROUPS;

//
// Decoder & Consist commands / Decoder parts
//

#define DECODER_COMMAND_RESET								0b00000
#define DECODER_COMMAND_FACTORY_TEST						0b00010
#define DECODER_COMMAND_RESERVED1							0b00100
#define DECODER_COMMAND_SET_DECODER_FLAG					0b00110
#define DECODER_COMMAND_RESERVED2							0b01000
#define DECODER_COMMAND_SET_ADVANDED_ADDRESSING				0b01010
#define DECODER_COMMAND_RESERVED3							0b01100
#define DECODER_COMMAND_ACK_REQUEST							0b01110

//
// Decoder & Consist commands / Consist parts
//

#define CONSIST_COMMAND_SET_SHORT_ADDRESS_AND_FORWARD		0b10010
#define CONSIST_COMMAND_SET_SHORT_ADDRESS_AND_REVERSE		0b10011

//
// Advanced commands
//

#define ADVANCED_COMMAND_SET_128_STEP_SPEED					0b11111
#define ADVANCED_COMMAND_SET_MAX_128_STEP_SPEED				0b11110
#define ADVANCED_COMMAND_ANALOG_CONTROL						0b11101

#define ADVANCED_COMMAND_ANALOG_CONTROL_SUBCOMMAND_VOLUME	0b00000001

//
// CV ACCESS sub-commands
//

typedef enum
{
	CV_ACCESS_SUBCOMMAND_RESERVED = 0b00,
	CV_ACCESS_SUBCOMMAND_VERIFY_BYTE = 0b01,
	CV_ACCESS_SUBCOMMAND_BIT_MANIPULATION = 0b10,
	CV_ACCESS_SUBCOMMAND_WRITE_BYTE = 0b11
} CV_ACCESS_SUBCOMMAND;

typedef enum
{
	CV_BIT_MANIPULATION_OPERATION_VERIFY = 0b0,
	CV_BIT_MANIPULATION_OPERATION_WRITE = 0b1
} CV_BIT_MANIPULATION_OPERATION;

#endif /*CONSTANTS_H_*/

